package com.rahasak.etcd4s

import cats.effect.IO
import com.ibm.etcd.client.EtcdClient
import com.rahasak.etcd4s.config.{Config, ServiceConfig}
import com.rahasak.etcd4s.repo.{EtcdRepoImpl, EtcdWatcherImpl}

import scala.util.Try

object Main extends App with ServiceConfig {

  implicit val ec = scala.concurrent.ExecutionContext.Implicits.global
  implicit val cs = IO.contextShift(ec)

  // repos
  val watcher = for {
    conf <- Config.load()
    cEtcd <- IO(EtcdClient.forEndpoint(conf.host, conf.port).withPlainText().build())
    eRepo <- IO(EtcdRepoImpl(cEtcd))
    eWat <- IO(EtcdWatcherImpl(cEtcd))
  } yield (eRepo, eWat)

  // init etcd repo
  val r1 = for {
    w <- watcher
    _ <- w._1.init(services)
  } yield None
  r1.unsafeRunSync()

  // init watcher
  val r2 = for {
    w <- watcher
    _ <- w._2.init(services)
  } yield None
  r2.unsafeRunSync()

  // wait
  waitForever()

  def waitForever(): Unit = {
    while (true) Try(this.synchronized(wait()))
  }

}
