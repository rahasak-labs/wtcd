package com.rahasak.etcd4s.repo

import cats.effect.{ContextShift, IO}
import com.google.protobuf.ByteString
import com.ibm.etcd.client.EtcdClient

import scala.collection.JavaConversions._
import scala.concurrent.{ExecutionContext, Future}

object EtcdRepoImpl {
  def apply(etcdClient: EtcdClient)(implicit ec: ExecutionContext, cs: ContextShift[IO]): EtcdRepoImpl = new EtcdRepoImpl(etcdClient)(ec, cs)
}

class EtcdRepoImpl(etcdClient: EtcdClient)(implicit ec: ExecutionContext, cs: ContextShift[IO]) extends EtcdRepo {

  val kvClient = etcdClient.getKvClient

  override def init(services: List[String]) = {
    services.foreach { s =>
      kvClient.put(ByteString.copyFromUtf8(s"services/$s"), ByteString.copyFromUtf8("running"))
        .sync()
    }
    IO(1L)
  }

  override def get(key: String): IO[Option[String]] = {
    IO.fromFuture(
      IO(
        Future {
          Option(
            kvClient.get(ByteString.copyFromUtf8(key))
              .async()
              .get()
              .getKvs(0)
          ).map(_.getValue.toStringUtf8)
        }
      )
    )
  }

  override def getRange(prefix: String): IO[List[String]] = {
    IO.fromFuture(
      IO(
        Future {
          kvClient.get(ByteString.copyFromUtf8(prefix))
            .asPrefix()
            .async()
            .get()
            .getKvsList
            .map(a => a.getValue.toStringUtf8).toList
        }
      )
    )
  }

  override def put(key: String, value: String): IO[Long] = {
    IO.fromFuture(
      IO(
        Future {
          kvClient.put(ByteString.copyFromUtf8(key), ByteString.copyFromUtf8(value))
            .async()
            .get()
          1
        }
      )
    )
  }

  override def delete(key: String): IO[Long] = {
    IO.fromFuture(
      IO(
        Future {
          kvClient.delete(ByteString.copyFromUtf8(key))
            .async()
            .get()
          1
        }
      )
    )
  }

}
