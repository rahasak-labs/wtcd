package com.rahasak.etcd4s.repo

import cats.effect.IO
import com.rahasak.etcd4s.protocol.Toggle

import scala.collection.mutable

object ToggleRepoImpl {

  val toggleStore = mutable.Map[String, Toggle]()

  def apply(etcdRepo: EtcdRepo) = {
    new ToggleRepoImpl(etcdRepo)
  }

}

class ToggleRepoImpl(etcdRepo: EtcdRepo) extends ToggleRepo {

  import ToggleRepoImpl._

  override def init(toggles: List[String]) = {
    // initialize toggle store
    IO(
      toggles.foreach(t =>
        etcdRepo.get(s"services/$t").unsafeRunSync().map(a => toggleStore.put(t, Toggle(t, a)))
      )
    )
  }

  override def putToggle(toggle: Toggle): IO[Option[Long]] = {
    IO(toggleStore.put(toggle.name, toggle).map(_ => 1))
  }

  override def getToggle(name: String): IO[Option[Toggle]] = {
    IO(toggleStore.get(name))
  }

}
