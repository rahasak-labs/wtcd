package com.rahasak.etcd4s.repo

import cats.effect.IO

trait EtcdRepo {

  def init(services: List[String]): IO[Long]

  def get(key: String): IO[Option[String]]

  def getRange(prefix: String): IO[List[String]]

  def put(key: String, value: String): IO[Long]

  def delete(key: String): IO[Long]

}
