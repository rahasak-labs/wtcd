package com.rahasak.etcd4s.repo

import cats.effect.IO
import com.google.protobuf.ByteString
import com.ibm.etcd.client.EtcdClient
import com.ibm.etcd.client.kv.WatchUpdate
import io.grpc.stub.StreamObserver

import scala.collection.JavaConversions._

object EtcdWatcherImpl {
  def apply(etcdClient: EtcdClient): EtcdWatcherImpl = {
    new EtcdWatcherImpl(etcdClient)
  }
}

class EtcdWatcherImpl(etcdClient: EtcdClient) extends EtcdWatcher {

  val kvClient = etcdClient.getKvClient

  val observer = new StreamObserver[WatchUpdate] {
    override def onNext(value: WatchUpdate): Unit = {
      value.getEvents.foreach { e =>
        val k = e.getKv.getKey.toStringUtf8.split("/").last
        val v = e.getKv.getValue.toStringUtf8
        println(s"$k: $v")
      }
    }

    override def onError(t: Throwable): Unit = {
    }

    override def onCompleted(): Unit = {
    }
  }

  override def init(services: List[String]) = {
    IO(
      services.foreach { s =>
        kvClient.watch(ByteString.copyFromUtf8(s"services/$s")).start(observer)
      }
    )
  }

  override def addWatcher(key: String) = {
    IO(
      kvClient.watch(ByteString.copyFromUtf8(key)).asPrefix.start(observer)
    )
  }

}
