package com.rahasak.etcd4s.repo

import cats.effect.IO
import com.rahasak.etcd4s.protocol.Toggle

trait ToggleRepo {

  def init(toggles: List[String]): IO[Unit]

  def putToggle(toggle: Toggle): IO[Option[Long]]

  def getToggle(name: String): IO[Option[Toggle]]

}
