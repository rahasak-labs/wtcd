package com.rahasak.etcd4s.repo

import cats.effect.IO
import com.ibm.etcd.client.kv.KvClient

trait EtcdWatcher {
  def init(services: List[String]): IO[Unit]

  def addWatcher(key: String): IO[KvClient.Watch]
}
