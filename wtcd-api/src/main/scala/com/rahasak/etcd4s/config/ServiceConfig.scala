package com.rahasak.etcd4s.config

import com.typesafe.config.ConfigFactory

import scala.collection.JavaConverters._

trait ServiceConfig {

  val config = ConfigFactory.load("service.conf")

  lazy val services = config.getStringList("services").asScala.toList

}
