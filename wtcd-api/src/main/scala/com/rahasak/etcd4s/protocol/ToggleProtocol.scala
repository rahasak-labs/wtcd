package com.rahasak.etcd4s.protocol

case class Toggle(name: String, enabled: String)

case class Service(name: String, status: String)

