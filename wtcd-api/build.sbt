name := "wtcd-api"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= {

  lazy val catsVersion = "2.0.0"
  lazy val circeVersion = "0.9.1"
  lazy val circeConfigVersion = "0.6.1"
  lazy val circeGenericVersion = "0.11.1"

  Seq(
    "org.typelevel"         %% "cats-effect"            % catsVersion,
    "io.circe"              %% "circe-core"             % circeVersion,
    "io.circe"              %% "circe-generic"          % circeVersion,
    "io.circe"              %% "circe-config"           % circeConfigVersion,
    "io.circe"              %% "circe-generic-extras"   % circeGenericVersion,
    "org.slf4j"             % "slf4j-api"               % "1.7.5",
    "ch.qos.logback"        % "logback-classic"         % "1.0.9",
    "com.github.mingchuno"  %% "etcd4s-core"            % "0.2.0",
    "com.github.mingchuno"  %% "etcd4s-akka-stream"     % "0.2.0",
    "com.ibm.etcd"          % "etcd-java"               % "0.0.14"
  )

}

resolvers ++= Seq(
  "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
)

assemblyMergeStrategy in assembly := {
  case PathList(ps @ _*) if ps.last endsWith ".properties" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".RSA" => MergeStrategy.discard
  case PathList(ps @ _*) if ps.last endsWith ".keys" => MergeStrategy.discard
  case PathList(ps @ _*) if ps.last endsWith ".logs" => MergeStrategy.discard
  case "module-info.class" => MergeStrategy.discard
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}
